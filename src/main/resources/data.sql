INSERT INTO `EasyBar`.`user` (id, `country`, `email`, `name`, `password`, `salary`, `street`, `street_number`, `surname`, `town`)
VALUES (1, 'Poland', 'admin@admin.com', 'admin', 'admin', 2000, 'adminowa', 3, 'admin', 'Wroclaw');
INSERT INTO `EasyBar`.`user` (id, `country`, `email`, `name`, `password`, `salary`, `street`, `street_number`, `surname`, `town`)
VALUES (2, 'Poland', 'user@user.com', 'user', 'user', 1000, 'userowa', 2, 'user', 'Wroclaw');
INSERT INTO `EasyBar`.`user` (id, `country`, `email`, `name`, `password`, `salary`, `street`, `street_number`, `surname`, `town`)
VALUES (3, 'Poland', 'komentator@komentator.com', 'komentator', 'komentator', 1500, 'komentatorowa', 2, 'komentator',
        'Wroclaw');

INSERT INTO `EasyBar`.`role` (id, role) VALUES (1, 'ADMIN');
INSERT INTO `EasyBar`.`role` (id, role) VALUES (2, 'USER');

INSERT INTO EasyBar.user_roles VALUES (1, 1);
INSERT INTO EasyBar.user_roles VALUES (2, 2);
INSERT INTO EasyBar.user_roles VALUES (3, 1);

INSERT INTO EasyBar.news (date, text, title, author_id) VALUES
  ('2017-05-20', 'Otwarto nowa siec restauracji szybkiej obslugi. Opiera sie na przelomowej aplikacji.', 'Nowa siec',
   3);

INSERT INTO EasyBar.news (date, text, title, author_id) VALUES
  ('2017-05-21', 'Koniecznie zobaczcie nasze nowe menu. Ludzie szaleja po zjedzenu przepysznego stejka', 'Nowe menu',
   3);

INSERT INTO EasyBar.news (date, text, title, author_id) VALUES
  ('2017-05-22',
   'Testowa tresc komentarza. Moze on być dosyc dlugi dlatego wklejam kawałek o szympansach:
   -Mimika szympansow jest waznym sposobem porozumiewania sie. Wydymanie warg jest oznaką przyjaznego powitania, podczas gdy usmiech odslaniający dolne zęby wskazuje na odczuwanie przyjemnosci. Obnażanie zebow wskazuje na agresje.
   -Szympansy są najbardziej inteligentne sposrod trzech gatunkow wielkich małp czlekoksztaltnych. W czasie grupowych wędrowek uzywają roznych zawolań.
   -Szympansy zjadaja bardzo roznorodne rosliny i owoce – do 300 gatunkow podczas całego roku. Mają równiez wielki apetyt: dorosly samiec szympansa moze zjesc 50 bananow na jeden posilek.'
    , 'jakis testowy naglowek', 3);

INSERT INTO EasyBar.menu (id, title) VALUES (1, "przekaska");
INSERT INTO EasyBar.menu (id, title) VALUES (2, "glowne");
INSERT INTO EasyBar.menu (id, title) VALUES (3, "napoje");

INSERT INTO EasyBar.meal (id, description, meal_type, price, weight) VALUES (1, 'Lasagne', 1, 14.4, 400);
INSERT INTO EasyBar.meal (id, description, meal_type, price, weight) VALUES (2, 'Spaghetti', 1, 18, 400);
INSERT INTO EasyBar.meal (id, description, meal_type, price, weight) VALUES (3, 'Stek wolowy', 1, 30, 300);
INSERT INTO EasyBar.meal (id, description, meal_type, price, weight) VALUES (4, 'Piers z kurczaka', 1, 16, 320);

INSERT INTO EasyBar.meal (id, description, meal_type, price, weight) VALUES (5, 'Krewetki', 1, 18, 150);
INSERT INTO EasyBar.meal (id, description, meal_type, price, weight) VALUES (6, 'Kanapeczki', 1, 8, 200);
INSERT INTO EasyBar.meal (id, description, meal_type, price, weight) VALUES (7, 'Sledzik', 1, 11, 180);

INSERT INTO EasyBar.meal (id, description, meal_type, price, weight) VALUES (8, 'Woda', 0, 3, 400);
INSERT INTO EasyBar.meal (id, description, meal_type, price, weight) VALUES (9, 'Cola', 0, 4, 330);
INSERT INTO EasyBar.meal (id, description, meal_type, price, weight) VALUES (10, 'Herbata', 0, 5, 220);
INSERT INTO EasyBar.meal (id, description, meal_type, price, weight) VALUES (11, 'Kawa', 0, 6, 220);

INSERT INTO EasyBar.meal (id, description, meal_type, price, weight) VALUES (12, 'Salatka', 2, 3, 400);
INSERT INTO EasyBar.meal (id, description, meal_type, price, weight) VALUES (13, 'Tofu', 2, 4, 330);
INSERT INTO EasyBar.meal (id, description, meal_type, price, weight) VALUES (14, 'Oreo', 2, 5, 220);
INSERT INTO EasyBar.meal (id, description, meal_type, price, weight) VALUES (15, 'Papryka', 2, 6, 220);

INSERT INTO EasyBar.menu_meals(menu_id, meals_id) VALUES (1,1);
INSERT INTO EasyBar.menu_meals(menu_id, meals_id) VALUES (1,2);
INSERT INTO EasyBar.menu_meals(menu_id, meals_id) VALUES (1,3);
INSERT INTO EasyBar.menu_meals(menu_id, meals_id) VALUES (1,4);

INSERT INTO EasyBar.menu_meals(menu_id, meals_id) VALUES (2,5);
INSERT INTO EasyBar.menu_meals(menu_id, meals_id) VALUES (2,6);
INSERT INTO EasyBar.menu_meals(menu_id, meals_id) VALUES (2,7);

INSERT INTO EasyBar.menu_meals(menu_id, meals_id) VALUES (3,8);
INSERT INTO EasyBar.menu_meals(menu_id, meals_id) VALUES (3,10);
INSERT INTO EasyBar.menu_meals(menu_id, meals_id) VALUES (3,11);


INSERT INTO EasyBar.menu_meals(menu_id, meals_id) VALUES (2,12);
INSERT INTO EasyBar.menu_meals(menu_id, meals_id) VALUES (2,13);
INSERT INTO EasyBar.menu_meals(menu_id, meals_id) VALUES (2,14);
