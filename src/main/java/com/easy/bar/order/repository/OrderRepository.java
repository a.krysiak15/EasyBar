package com.easy.bar.order.repository;

import com.easy.bar.order.model.Order;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by lukasz on 6/11/17.
 */
public interface OrderRepository extends CrudRepository<Order, Long> {

}
