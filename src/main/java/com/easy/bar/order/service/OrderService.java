package com.easy.bar.order.service;

import com.easy.bar.order.model.Order;

import java.util.List;

/**
 * Created by lukasz on 6/11/17.
 */
public interface OrderService {
    Order addOrder(Order order);
    void deleteOrder(Integer id);
    void updateOrder(Order order);
    List<Order> getOrders();
}
