package com.easy.bar.user.service;

import com.easy.bar.config.CoreConfig;
import com.easy.bar.config.EncryptionHelper;
import com.easy.bar.user.model.User;
import com.easy.bar.user.repository.UserRepository;
import com.google.common.collect.Lists;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by Adam Krysiak on 16.03.17.
 */

@Service
public class UserServiceImpl implements UserService {

    @Value("${user.sal:salt}")
    private String encryptionSalt;
    @Autowired
    private ModelMapper mapper;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CoreConfig config;

    @Override
    public Long saveUser(User user) {
        User map = mapper.map(user, User.class);
        map.setPassword(EncryptionHelper.encodePassword(map.getPassword(),encryptionSalt));
        return userRepository.save(map).getId();
    }

    @Override
    public List<User> getAllUsers() {
        return Lists.newArrayList(userRepository.findAll());
    }

    @Override
    public User getUserById(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException("user not found in database"));
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByName(username).orElseThrow(() -> new UsernameNotFoundException("user not found in database"));

        List<SimpleGrantedAuthority> authorities = user
                .getRoles()
                .stream()
                .map(e -> new SimpleGrantedAuthority(e.getRole()))
                .collect(Collectors.toList());
        return new org.springframework.security.core.userdetails.User(user.getName(), user.getPassword(), authorities);
    }

}
