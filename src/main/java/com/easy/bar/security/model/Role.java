package com.easy.bar.security.model;

import com.easy.bar.user.model.User;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by Adam Krysiak on 06.05.17.
 */

@Entity
@Getter
@Setter
public class Role implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    private String role;

    @ManyToMany(mappedBy = "roles")
    @JsonBackReference
    private Set<User> users;

}
