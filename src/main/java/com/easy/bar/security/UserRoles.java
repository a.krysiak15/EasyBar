package com.easy.bar.security;

/**
 * Created by Adam Krysiak on 23.04.17.
 */
public class UserRoles {
    private UserRoles(){}
    public static final String ADMIN = "ADMIN";
    public static final String USER = "USER";
}
