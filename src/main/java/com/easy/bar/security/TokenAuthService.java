package com.easy.bar.security;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Adam Krysiak on 23.04.17.
 */
public interface TokenAuthService {
    void addAuthentication(HttpServletResponse response, String username);
    Authentication getAuthentication(HttpServletRequest request);
}
