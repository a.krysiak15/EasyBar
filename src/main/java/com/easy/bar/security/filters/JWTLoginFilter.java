package com.easy.bar.security.filters;

import com.easy.bar.auth.AuthenticationCredentials;
import com.easy.bar.security.TokenAuthService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Adam Krysiak on 23.04.17.
 */
public class JWTLoginFilter extends AbstractAuthenticationProcessingFilter {

    private final TokenAuthService tokenAuthService;

    public JWTLoginFilter(String defaultFilterProcessesUrl, AuthenticationManager manager, TokenAuthService authService) {
        super(defaultFilterProcessesUrl);
        tokenAuthService = authService;
        setAuthenticationManager(manager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException, IOException, ServletException {
        AuthenticationCredentials credentials = mapRequestToCredentials(request);
        UsernamePasswordAuthenticationToken token = createToken(credentials);
        return getAuthenticationManager().authenticate(token);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        tokenAuthService.addAuthentication(response, authResult.getName());

    }

    private AuthenticationCredentials mapRequestToCredentials(HttpServletRequest request) throws IOException {
        return new ObjectMapper().readValue(request.getInputStream(), AuthenticationCredentials.class);
    }

    private UsernamePasswordAuthenticationToken createToken(AuthenticationCredentials authenticationCredentials) {
        return new UsernamePasswordAuthenticationToken(authenticationCredentials.getUsername(), authenticationCredentials.getPassword());
    }
}
