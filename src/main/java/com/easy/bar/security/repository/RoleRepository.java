package com.easy.bar.security.repository;

import com.easy.bar.security.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Adam Krysiak on 06.05.17.
 */
public interface RoleRepository extends JpaRepository<Role, Long>{
}
