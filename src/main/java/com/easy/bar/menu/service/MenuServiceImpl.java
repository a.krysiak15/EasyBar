package com.easy.bar.menu.service;

import com.easy.bar.menu.model.Menu;
import com.easy.bar.menu.repository.MenuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lukasz on 5/27/17.
 */
@Service
public class MenuServiceImpl implements MenuService {
    private final MenuRepository menuRepository;

    @Autowired
    public MenuServiceImpl(MenuRepository menuRepository) {
        this.menuRepository = menuRepository;
    }

    @Override
    public Menu addMenu(Menu menu) {
        return menuRepository.save(menu);
    }

    @Override
    public void deleteMenu(Integer id) {
        menuRepository.delete(id);
    }

    @Override
    public Menu updateMenu(Menu menu) {
        return menuRepository.save(menu);
    }

    @Override
    public Menu getMenu(Integer id) {
        return menuRepository.findOne(id);
    }

    @Override
    public List<Menu> getMenus() {
        return javaslang.collection.List
        .ofAll(menuRepository.findAll()).toJavaList();
    }
}
