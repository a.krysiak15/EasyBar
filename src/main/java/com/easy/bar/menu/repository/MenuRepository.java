package com.easy.bar.menu.repository;

import com.easy.bar.menu.model.Menu;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by lukasz on 5/27/17.
 */
public interface MenuRepository extends CrudRepository<Menu,Integer> {
}
