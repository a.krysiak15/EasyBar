package com.easy.bar.menu.controller;

import com.easy.bar.menu.model.Menu;
import com.easy.bar.menu.service.MenuServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;
import java.util.Optional;

/**
 * Created by lukasz on 5/27/17.
 */
@RequestMapping("/menu")
public class MenuController {
    @Autowired
    private MenuServiceImpl menuService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Menu addMenu(@RequestBody Menu menu){
        return menuService.addMenu(menu);
    }

    @PutMapping
    public Menu updateMenu(@RequestBody Menu menu) {
        return  menuService.updateMenu(menu);
    }

    @GetMapping("/{id}")
    public Menu getMenu(@PathVariable Integer id) {
        return menuService.getMenu(id);
    }

    @GetMapping
    public List<Menu> getMenus() {
        return menuService.getMenus();
    }

    @DeleteMapping("/{id}")
    public void deleteOffer(@PathVariable Integer id) {
        menuService.deleteMenu(id);
    }
}
