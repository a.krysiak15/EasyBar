package com.easy.bar.meal.service;

import com.easy.bar.meal.model.Meal;

import java.util.List;

/**
 * Created by lukasz on 5/27/17.
 */
public interface MealService {
    Meal addMeal(Meal meal);
    void deleteMeal(Integer id);
    Meal updateMeal(Meal meal);
    Meal getMeal(Integer id);
    List<Meal> getMeals();

}
