package com.easy.bar.meal.repository;

import com.easy.bar.meal.model.Meal;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by lukasz on 5/27/17.
 */
public interface MealRepository extends CrudRepository<Meal,Integer> {

}
