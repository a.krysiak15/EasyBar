package com.easy.bar.config;

import org.apache.commons.codec.digest.DigestUtils;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * Created by lukasz on 4/20/17.
 */
@Component
public class CoreConfig {


    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
