package com.easy.bar.config;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Created by lukasz on 4/20/17.
 */
public final class EncryptionHelper {
    public static String encodePassword(String field, String salt) {
        return DigestUtils.sha256Hex(field + salt);
    }
}
