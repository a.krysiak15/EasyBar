package com.easy.bar.news.model;

import lombok.*;

import java.util.Date;

/**
 * Created by Adam Krysiak on 16.06.17.
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class NewsResponse {
    private String title;
    private String text;
    private Date date;
    private Long authorId;
    private String authorName;
    private String authorSurname;

    public NewsResponse(News news) {
        this.title = news.getTitle();
        this.text = news.getText();
        this.date = news.getDate();
        this.authorId = news.getAuthor().getId();
        this.authorName = news.getAuthor().getName();
        this.authorSurname = news.getAuthor().getSurname();
    }
}
