package com.easy.bar.news.controller;

import com.easy.bar.news.model.News;
import com.easy.bar.news.model.NewsRequest;
import com.easy.bar.news.model.NewsResponse;
import com.easy.bar.news.service.NewsService;
import com.easy.bar.user.model.User;
import com.easy.bar.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Adam Krysiak on 18.04.17.
 */
@RestController
@RequestMapping(path = "/news")
public class NewsController {

    private final NewsService newsService;
    private final UserService userService;

    @Autowired
    public NewsController(NewsService newsService, UserService userService) {
        this.newsService = newsService;
        this.userService = userService;
    }

    @GetMapping
    List<NewsResponse> getAll() {
        return newsService.getAll().stream().map(NewsResponse::new).collect(Collectors.toList());
    }

    @PutMapping
    News update(@RequestParam News toUpdate) {
        return newsService.save(toUpdate);
    }

    @PostMapping(path = "/add")
    Integer add(@RequestBody NewsRequest newsRequest) {
        User user = userService.getUserById(newsRequest.getAuthorId());
        return newsService.add(new News(newsRequest, user));
    }

    @GetMapping("/{id}")
    public News get(@PathVariable Integer id) {
        return newsService.get(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        newsService.delete(id);
    }
}
