package com.easy.bar.news.service;

import com.easy.bar.news.model.News;

import java.util.Date;
import java.util.List;

/**
 * Created by Adam Krysiak on 18.04.17.
 */
public interface NewsService {
    Integer add(News news);
    List<News> getNewsAfter(Date startDate);
    List<News> getNewsBefore(Date endDate);
    List<News> getNewsFrom(Date exactDate);
    List<News> getAll();
    News save(News news);
    void delete(Integer id);
    News get(Integer id);
}
