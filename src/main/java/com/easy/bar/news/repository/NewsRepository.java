package com.easy.bar.news.repository;

import com.easy.bar.news.model.News;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by Adam Krysiak on 18.04.17.
 */
public interface NewsRepository extends CrudRepository<News, Integer>{
    List<News> findNewsByDateBefore(Date date);
    List<News> findNewsByDateAfter(Date date);
    List<News> findNewsByDate(Date date);
}
