package com.easy.bar.meal;

import com.easy.bar.meal.model.Meal;
import com.easy.bar.meal.model.MealType;
import com.easy.bar.meal.repository.MealRepository;
import com.easy.bar.meal.service.MealServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.when;


/**
 * Created by lukasz on 5/29/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MealServiceTest {
    @Mock
    private MealRepository mealRepository;

    @InjectMocks
    private MealServiceImpl mealService;

    @Test
    public void shouldAddMeal() {
        //given
        when(mealRepository.findAll()).thenReturn(getTestMeals());
        //when
        List<Meal> meals = mealService.getMeals();
        //than
        assertThat(meals).hasSize(2);
        assertThat(meals).containsAll(getTestMeals());
    }

    private List<Meal> getTestMeals() {
        return Arrays.asList(
                Meal.builder()
                        .mealType(MealType.DRINK)
                        .description("Zupa 1")
                        .price(1124.22f)
                        .build(),
                Meal.builder()
                        .mealType(MealType.FOOD)
                        .description("Zupa szczawiowa")
                        .price(12.22f)
                        .build());
    }

}
