package com.easy.bar.menu;

import com.easy.bar.meal.model.Meal;
import com.easy.bar.meal.model.MealType;
import com.easy.bar.meal.repository.MealRepository;
import com.easy.bar.meal.service.MealServiceImpl;
import com.easy.bar.menu.model.Menu;
import com.easy.bar.menu.repository.MenuRepository;
import com.easy.bar.menu.service.MenuServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * Created by lukasz on 5/29/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MenuServiceTest {
    @Mock
    private MenuRepository menuRepository;

    @InjectMocks
    private MenuServiceImpl menuService;

    @Test
    public void shouldGetMenu() {
        //given
        when(menuRepository.findAll()).thenReturn(Collections.singletonList(getTestMenu()));
        //when
        List<Menu> menus = menuService.getMenus();
        //than
        assertThat(menus).hasSize(1);
        assertThat(menus.get(0).getMeals()).containsAll(getTestMealList());
    }

    private Menu getTestMenu() {
        return Menu.builder()
                .meals(getTestMealList())
                .title("Menu Zup")
                .build();
    }

    private List<Meal> getTestMealList() {
        return Arrays.asList(
                Meal.builder()
                        .mealType(MealType.FOOD)
                        .description("Zupa szczawiowa")
                        .price(12.22f)
                        .weight(300)
                        .build(),
                Meal.builder()
                        .mealType(MealType.VEGAN)
                        .description("Zupa Veganowa")
                        .weight(10)
                        .price(12.22f)
                        .build());
    }
}
