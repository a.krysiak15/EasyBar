Using services requires authentication.
This application uses JWT.
To get it You need to send post to "http://localhost:8080/login"
with body "{"username":"admin", "password":"admin"}".
This credentials are for default user, that needs to be removed in future.
In response you receive token:
"Authorization: eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTQ5NDYyNjQwMH0.851F_yFxoGtFn7rRvJeKQ7vrTe7QmBxXgqAqZxdNUq0"
To use services you need to pass thas token as header
name: Authorization
value: eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTQ5NDU0MDAwMH0.fzsFP6Z3P1iLGbDlGXViaX_n0ExeuKFBvetgJ27e5C4